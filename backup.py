import os
import sys
import json
import glob
import shutil

def copy (file, config):
	print("from file: "+file)
	for to_dir in config['to_dir']:
		try:
			shutil.copy2(file,os.path.join(to_dir));
		except:
			print("copy err on copy "+file)
			print("to "+to_dir)
			sys.exit(4);
	return True

def recursive(dir,config):
	try:
		fileList = os.listdir(dir)
	except os.error:
			print ("invalind input folder")
			print (os.error)
			sys.exit(3);
	for file in fileList:
		if not os.path.isdir(os.path.join(dir,file)):
			(fileName, ext) = os.path.splitext(file)
			if (ext) in config['format']:
				copy(os.path.join(dir,file), config)
		else:
			recursive(os.path.join(dir,file),config)
	return True

def backup(config):
	'''main function'''
	# print (config['to_dir'])
	print ("FROM DIR:")
	for dir in config['from_dir']:
		print (dir)		
		recursive(dir,config)
	print ("TO DIR:")
	for dir in config['to_dir']:
		print (dir)
	print ("FORMATS:")
	for format in config['format']:
		print (format)
	return True
	return False

def loadJsonFromFile(filename = ".config"):
	'''load JSON from file and return data from it'''
	with open(filename, 'r', encoding='utf-8') as f:
		try:
			entry = json.load(f)
		except:
			print ("config err")
			sys.exit(2);
	return entry

if __name__ == '__main__':
	try:
		config = loadJsonFromFile("backup.config")
	except:
		print("can't load config file")
		sys.exit(-1);
	print("load config:")
	# print(config)
	# print(loadJsonFromFile.__doc__)
	if (backup(config)):
		print ('end of backup')
		sys.exit(0);
	else:
		print ('backup crashed')
		sys.exit(1)